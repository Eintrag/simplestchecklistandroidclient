package todaylayout.simplestchecklist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class CheckListActivity extends AppCompatActivity {
    List<CheckBox> checkBoxes = new ArrayList<CheckBox>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist);
    }

    public void onClickAddNewCheckBoxItem(View view) {
        CheckBox checkBox = new CheckBox(getApplicationContext());
        checkBox.setText(getUserInputText());
        checkBox.setTextColor(Color.BLACK);
        checkBoxes.add(checkBox);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layoutForCheckboxes);
        linearLayout.addView(checkBox);
    }

    protected String getUserInputText() {
        EditText editText = (EditText) findViewById(R.id.editText);
        String inputText = editText.getText().toString();
        editText.setText("");
        return inputText;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.checklistmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_item_uncheckAll: {
                askUncheckAllCheckboxes();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    protected void askUncheckAllCheckboxes() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.uncheckAllQuestion);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                uncheckAllCheckboxes();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void uncheckAllCheckboxes() {
        for (CheckBox checkbox : checkBoxes) {
            checkbox.setChecked(false);
        }
    }
}
